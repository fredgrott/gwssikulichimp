GWSSikuliChimp
---

Forked from SikuliChimp at GoogleCode.

# Useage

Dependencies are bootclasspath'd so you need to include sikuli-script.jar, chimpchat.jar, 
guavalib.jar, ddmlib.jar, and sdklib.jar in your project using GWSSikuliChimp jar.


# License

Apache License 2.0