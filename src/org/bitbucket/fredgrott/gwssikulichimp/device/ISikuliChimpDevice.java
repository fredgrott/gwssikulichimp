package org.bitbucket.fredgrott.gwssikulichimp.device;

import java.io.IOException;

import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.TouchPressType;

// TODO: Auto-generated Javadoc
/**
 * The Interface ISikuliChimpDevice.
 *
 * @author rohit
 */
public interface ISikuliChimpDevice extends IChimpDevice {

    /**
     * Touch.
     *
     * @param imageName the image name
     * @param pressType the press type
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void touch(String imageName,TouchPressType pressType) throws IOException;
    
    /**
     * Drag.
     *
     * @param from the from
     * @param to the to
     * @param steps the steps
     * @param distance the distance
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void drag(String from, String to, int steps, long distance) throws IOException;
    
    /**
     * Wait for.
     *
     * @param imageName the image name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void waitFor(String imageName) throws IOException;
    
    /**
     * Wait for.
     *
     * @param imageName the image name
     * @param timeout the timeout
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void waitFor(String imageName, int timeout) throws IOException;
    
    /**
     * Unlock device.
     */
    public void unlockDevice();
    
    /**
     * Lock device.
     */
    public void lockDevice();
    
    /**
     * Start activity.
     *
     * @param component the component
     */
    public void startActivity(String component);
}