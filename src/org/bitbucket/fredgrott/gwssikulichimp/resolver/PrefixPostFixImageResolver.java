package org.bitbucket.fredgrott.gwssikulichimp.resolver;

// TODO: Auto-generated Javadoc
/**
 * The Class PrefixPostFixImageResolver.
 *
 * @author rohit
 */
public class PrefixPostFixImageResolver implements IImageResolver {

    /** The prefix. */
    private String prefix;
    
    /** The post fix. */
    private String postFix;
    
    /**
     * Instantiates a new prefix post fix image resolver.
     *
     * @param prefix the prefix
     * @param postFix the post fix
     */
    public PrefixPostFixImageResolver(String prefix,String postFix){
        this.prefix=prefix;
        this.postFix=postFix;
    }
    /**
     * Given that the imageName is "button", this method returns
     * "screenshots/button.png"
     * 
     * @param imageName
     *            Name of the Image
     * @return screenhots/<<imageName>>.png
     */
    @Override
    public String getImagePath(String imageName) {
        return prefix + imageName + postFix;
    }

}
