package org.bitbucket.fredgrott.gwssikulichimp.resolver;

// TODO: Auto-generated Javadoc
/**
 * The Interface IImageResolver.
 *
 * @author rohit
 */
public interface IImageResolver {

    /**
     * Gets the image path.
     *
     * @param imageName the image name
     * @return the image path
     */
    public String getImagePath(String imageName);
}